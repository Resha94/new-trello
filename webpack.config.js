var path = require('path');
var webpack = require('webpack');
var context = path.resolve(__dirname, './app');
module.exports = {
    context: context,
    entry: path.resolve(context,'./index.js') ,
    output : {
        filename : 'bundle.js'
    },
    devtool: "cheap-inline-module-source-map",
    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },
    resolveLoader: {
        modulesDirectories: ['node_modules'],
        modulesTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },
    module : {
      loaders : [
          {test : /\.js$/, exclude: /(node_modules)/, loader : 'babel?presets[]=es2015' }
      ],   noParse: /angular\/angular.js/
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin()

    ],
    devServer : {
        hot:true
    }
};