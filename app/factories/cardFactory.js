var _ = require('lodash');
export default function (ngModule) {
    ngModule.factory('cardFactory', function () {
        let service = {};
        let cards = [
            {
                id: 1,
                description: 'Fix bug in in in player',
                list_id: 1
            }, {
                id: 2,
                description: 'Add feature with D3',
                list_id: 2
            }, {
                id: 3,
                description: 'Learn AngularJs',
                list_id: 3
            }, {
                id: 4,
                description: 'Learn REACT',
                list_id: 3
            }
        ];

        service.getCards = (list) => {
            return _.filter(cards, {list_id: list.id});
        };
        service.createCard = (id, cardDescription) => {
            cards.push({
                id: _.uniqueId('card_'),
                description: cardDescription,
                list_id: id
            });
        };
        service.deleteCard = (card) => {
            return _.pull(cards, card);
        };
        service.updateCard = (updatingCard) => {
            let card = cards[_.findKey(cards, {id : updatingCard.id})];
            card.description= updatingCard.description;
            card.list_id = updatingCard.list_id;
        };
        return service;
    });
}
