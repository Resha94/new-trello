import cardFactory from './cardFactory';
import listsFactory from './listsFactory';

export default (ngModule) => {
    cardFactory(ngModule);
    listsFactory(ngModule);
}