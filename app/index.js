var angular = require('angular');
var ngModule = angular.module('app',[]);
import controllers from './controllers/index'
import factories from './factories/index'
import directives from './directives/closeEditing'

controllers(ngModule);
factories(ngModule);
directives(ngModule);
