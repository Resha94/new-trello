export default function (ngModule) {
    ngModule.controller('cardCtrl', function (cardFactory) {
        this.isEditing = false;
        this.editingCard = null ;
        this.deleteCard = (card) =>{
            cardFactory.deleteCard(card);
            this.cardDescription='';
        };
        this.editCard= (card) =>{
            this.isEditing= true;
            this.editingCard = angular.copy(card);
        };
        this.updateCard = () => {
            cardFactory.updateCard(this.editingCard);
            this.editingCard = null;
            this.isEditing=false;
        }
    });
}