import listGroupCtrl from './listGroupCtrl';
import listCtrl from './listCtrl';
import cardCtrl from './cardCtrl';

export default  (ngModule) => {
    listGroupCtrl(ngModule);
    listCtrl(ngModule);
    cardCtrl(ngModule);
};