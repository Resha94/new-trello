export default function (ngModule) {
    ngModule.controller('listCtrl', function (listsFactory, cardFactory) {
        this.deleteList = (list) => {
            listsFactory.deleteList(list);
        };
        this.getCards = (list)=> cardFactory.getCards(list);

        this.createCard = (list)=> {
            cardFactory.createCard(list.id, this.cardDescription);
            this.cardDescription = '';
        }
    });
}

