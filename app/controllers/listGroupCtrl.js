export default function (ngModule) {
    ngModule.controller('listGroupCtrl', function (listsFactory) {
        this.lists = listsFactory.getLists();
        this.addList= function () {
            listsFactory.addList(this.listName);
            this.listName='';
        };
    });
}