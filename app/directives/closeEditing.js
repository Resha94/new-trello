export default (ngModule) => {
    ngModule.directive('closeEditing', () => {
        const KEYS = {
            ESCAPE: 27
        };
        return {
            scope: {
                isEditing: '='
            },
            link(scope, element, attrs) {
                element.on('keyup', (e)=> {
                    if (_.isEqual(e.keyCode, KEYS.ESCAPE)) {
                        scope.isEditing = false;
                        scope.$apply();
                    }
                });
            }
        }
    });
}